import datetime
import os
import sys
import csv
import time
import tqdm
import shutil
from copy import deepcopy
import re

from tools.Logger import Logger
from tools.paths import paths
from tools.config import config
from tools.params import params
from functions import *

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from multiprocessing import Pool


def main_runner(logger=None, data=None):
    run_init = True
    if run_init:
        run_idx = data['idx']
        paths = data['paths']
        config = data['config']
        params = data['params']

        create_results_paths = True
        if create_results_paths:
            paths_dict = dict()
            paths_dict['result_path'] = paths['result_path']
            for k, folder_path in paths_dict.iteritems():
                if not os.path.exists(folder_path):
                    os.makedirs(folder_path)

            del create_results_paths

    # Data gathering
    data_gathering = True
    if data_gathering:
        # Output: v1
        logger.log_print("Creating data for example")
        v1 = range(params['input_bound'])

        del data_gathering

    # Analyze data
    analyze_data = True
    if analyze_data:
        # Analyze data
        create_inputs = True
        if create_inputs:

            create_global_inputs = True
            if create_global_inputs:
                global_input_vector = dict()
                global_input_vector['df'] = 'example_data_that_is_always_relevant'
                global_input_vector['params'] = params
                global_input_vector['config'] = config
                global_input_vector['paths'] = paths

                del create_global_inputs

            create_inputs_vector = True
            if create_inputs_vector:
                input_vector = list()
                for idx, val in enumerate(v1):
                    c_pckg = dict()
                    c_pckg['val'] = val
                    c_pckg['key'] = idx
                    input_vector.append(c_pckg)

                del create_inputs_vector

            del create_inputs

        run_on_inputs = True
        if run_on_inputs:
            logger.log_print("Running on {} mode".format(config['workload_handling']))
            result_vector = list()
            tqdm_msg = "Applying function on inputs\t\t"
            if config['workload_handling'] == 'parallel':
                logger.log_print("Opening worker's pool")
                pool = Pool(initializer=calculation_worker_init, initargs=(logger, global_input_vector))
                results = pool.imap(calculation_worker, input_vector)
                time.sleep(0.1)
                for item_id in tqdm.tqdm(range(len(input_vector)), desc=tqdm_msg):
                    res = results.next()
                    result_vector.append(res)

                pool.close()
                pool.join()
                time.sleep(0.1)
            elif config['workload_handling'] == 'concurrent':

                calculation_worker_init(logger, global_input_vector)
                logger.ena_logger_console(False)
                time.sleep(0.1)
                for item_id in tqdm.tqdm(range(len(input_vector)), desc=tqdm_msg):
                    res = calculation_worker(input_vector[item_id])
                    result_vector.append(res)
                time.sleep(0.1)
                logger.ena_logger_console(True)
            else:
                logger.log_print("ERROR: bad option for workload_handling: {}".format(config.workload_handling))

            del run_on_inputs

        handle_results = True
        if handle_results:
            logger.log_print("Extracting data from results")
            tlist = sorted(result_vector, key=lambda p: p['val'])
            x_items = list()
            y_items = list()
            for item in tlist:
                x_items.append(item['val'])
                y_items.append(item['ret'])

            del handle_results

        del analyze_data

    # Export results
    export_results = True
    if export_results:
        results_file_name = 'results_as_graph'

        plot_results = True
        if plot_results:
            plt.plot(x_items, y_items)
            image_path = os.path.join(paths['result_path'], '{}.{}'.format(results_file_name, 'jpeg'))
            logger.log_print("Result graph path: {}".format(image_path))
            plt.savefig(image_path)
            plt.close()

            del plot_results

        to_csv_results = True
        if to_csv_results:
            csv_path = os.path.join(paths['result_path'], '{}.{}'.format(results_file_name, 'csv'))
            logger.log_print("Result graph (as CSV) path: {}".format(csv_path))
            with open(csv_path, 'wb') as csvfile:
                filewriter = csv.writer(csvfile)
                filewriter.writerow(['x', 'f(x)'])
                for idx in range(len(x_items)):
                    cx = x_items[idx]
                    cy = y_items[idx]
                    filewriter.writerow([cx, cy])

            del to_csv_results

        del export_results


if __name__ == '__main__':
    # Old data removal
    clear_paths = True
    if clear_paths:
        paths_to_remove = dict()

        pattern = os.path.basename(paths['result_path']).replace('@', '[0-9]+')
        for p in os.listdir(paths['work_path']):
            if re.search(pattern, p) is not None:
                paths_to_remove[p] = os.path.join(paths['work_path'], p)

        paths_to_remove['summary'] = paths['summary']
        for k, dir_path in paths_to_remove.iteritems():
            if os.path.exists(dir_path):
                all_items_to_remove = [os.path.join(dir_path, f) for f in os.listdir(dir_path)]
                for item_to_remove in all_items_to_remove:
                    if os.path.exists(item_to_remove) and not os.path.isdir(item_to_remove):
                        os.remove(item_to_remove)
                    else:
                        shutil.rmtree(item_to_remove)
                shutil.rmtree(dir_path)

        del clear_paths, paths_to_remove

    # path verification
    verify_paths_exist = True
    if verify_paths_exist:
        paths_dict = dict()
        paths_dict['Root'] = paths['work_path']
        paths_dict['src'] = paths['src_path']
        paths_dict['summary'] = paths['summary']
        for k, folder_path in paths_dict.iteritems():
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)

        del verify_paths_exist, paths_dict

    # Define Logger
    define_logger = True
    if define_logger:
        # Output: logger
        logger = Logger()
        logger.initThread(paths['summary'] + '\\report.txt')
        logger.log_print("START OF CODE")
        logger.log_print()

        del define_logger

    # Set random seed
    set_random_seed = True
    if set_random_seed:
        if config['random_seed'] is None:
            random_seed = np.random.randint(1, sys.maxint)
        else:
            random_seed = config.random_seed
        np.random.seed(random_seed)
        logger.log_print("Random seed in use: {}".format(random_seed))
        logger.log_print()

        del set_random_seed, random_seed

    # Generate inputs
    generate_inputs = True
    if generate_inputs:
        inputs_vector = list()
        for jidx in range(5):
            jdata = dict()
            jdata['idx'] = jidx
            jdata['params'] = deepcopy(params)
            jdata['config'] = deepcopy(config)
            jdata['paths'] = deepcopy(paths)

            jdata['paths']['result_path'] = jdata['paths']['result_path'].replace('@', str(jidx))
            jdata['config']['random_seed'] = np.random.randint(1, sys.maxint)
            inputs_vector.append(jdata)

        del jidx
        del generate_inputs

    # Run code
    for jidx, jdata in enumerate(inputs_vector):
        logger.log_print("Running main ({}/{})".format(jidx + 1,
                                                       len(inputs_vector)))

        main_runner(logger, jdata)

    # End of code
    end_of_code = True
    if end_of_code:
        logger.log_print()
        logger.system_check("End of code system check")
        logger.log_print("END OF CODE.")
        logger.log_close()

        del end_of_code
