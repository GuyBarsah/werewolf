import os

paths = dict()

paths['work_path'] = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
paths['src_path'] = os.path.join(paths['work_path'], 'src')
paths['summary'] = os.path.join(paths['work_path'], 'summary')
paths['result_path'] = os.path.join(paths['work_path'], 'results_@')
