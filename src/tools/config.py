config = dict()

# type of workload handling (concurrent, parallel)
config['workload_handling'] = 'concurrent'

# Random seed, if "None" a random seed will be randomized
config['random_seed'] = None
