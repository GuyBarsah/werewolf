import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from multiprocessing import Pool
import datetime
import os
import sys
import csv
import time
import tqdm
import shutil


def calculation_worker_init(loggert, pckg):
    global logger
    global params
    global config
    global paths

    global df
    logger = loggert

    df = pckg['df']
    params = pckg['params']
    config = pckg['config']
    paths = pckg['paths']


def calculation_worker(pckg):
    val = pckg['val']
    key = pckg['key']

    ret = np.sqrt(val)

    ret_pckg = dict()
    ret_pckg['val'] = val
    ret_pckg['key'] = key
    ret_pckg['ret'] = ret
    return ret_pckg
