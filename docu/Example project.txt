Created: Guy Barash
Western Digital
2018

This file is a template and and example for scripts and tools. 

Folders:

Work path:
the main path is the folder in which the project is stored. It can change and it should be updated in the "config.py" file before running. it contains:
 "src" folder
 "docu" folder
 and any result folder should be generated here as well (not files! just the results folder)
 
Docu:
Here the documentation for you project is stored, you can place PPTs, excels, .doc files etc. 
(not code files)

src:
The heart of the project, here all the code is stored, it should contain at least one subfolder named "tools".
this folder will always contain at least the main py file, and can contain other code files.



Files:
the main file: (name can vary)
should be placed in the src folder, this is the file you must run to start your script

"Config.py" file:
should be placed in [work_path]/src/tools, contains all the numeric parameters and the run configurations (parallel / concurent for example). Can be split into more than 1 file (for example one for parameters, one for config etc.)

"paths.py" file:
This is the file that allows you to reach other parts of your project and outside of it, 
it contains as the first line "work_path = XXX" where XXX is the path to the work path. this line is the only line that should change when you move your project between machines or folders. 
the other parameters are:
src_path: which should be "work_path + '\\src' "
and other paths you may want to add. always connect subfolder to the work path, for example:
result_path = work_path + '\\Results' ,this way if you move your project, only the "work_path" should change.